Indiana_odyssey
===============


Interactive web map that show the spanish locations of the Indiana Jones and the Last Crusade movie.

![indy](http://mappingco.github.io/Indiana_odyssey/photos/web.png)

Made with a early version of: https://github.com/CartoDB/odyssey.js